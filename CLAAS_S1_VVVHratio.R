
#+
# :Description:
#
# :AUTHOR: Katarzyna Ewa Lewinska
# :DATE: 18 May 2018
# :VERSION: 1.0
#-


t1=Sys.time()

library(raster)
library(rgdal)
library(sp)
library(ggplot2)
library(GGally)


# in.VV = 'L:/presales/ClaasRGF/S1/2017/11_1/VVdB_stack3-7_32630.tif'
# in.VH = 'L:/presales/ClaasRGF/S1/2017/11_1/VHdB_stack3-7_32630.tif'

in.VV = 'L:/presales/ClaasRGF/S1/2017/11_1/VV_stack_2016-2017_32630.tif'
in.VH = 'L:/presales/ClaasRGF/S1/2017/11_1/VH_stack_2016-2017_32630.tif'

in.VV.d = brick(in.VV)
in.VH.d = brick(in.VH)


in.shp = 'L:/presales/ClaasRGF/AOI/UK_DemoWGS84.shp'
shp.extend = readOGR(dsn=in.shp) # shp object
shp_utm <- spTransform(shp.extend, crs(in.VV.d))

in.VV.d <- crop(in.VV.d, extent(shp_utm))
in.VH.d <- crop(in.VH.d, extent(shp_utm))

# out.name = 'L:/presales/ClaasRGF/04_DataProducts/S1/VV_stack_32630_subset.tif'
# writeRaster(in.VV.d, filename=out.name, format='GTiff', Ebandorder='TIF', datatype='FLT4S', overwrite=TRUE, crs=in.VV.d@crs)
# 
# out.name = 'L:/presales/ClaasRGF/04_DataProducts/S1/VH_stack_32630_subset.tif'
# writeRaster(in.VH.d, filename=out.name, format='GTiff', Ebandorder='TIF', datatype='FLT4S', overwrite=TRUE, crs=in.VV.d@crs)

SAR.stack = raster::stack()

for (i in 1:in.VV.d@data@nlayers) {
  VHVV.i = in.VH.d[[i]]/in.VV.d[[i]]
  SAR.stack = raster::stack(SAR.stack,VHVV.i)
}

out.name = 'L:/presales/ClaasRGF/04_DataProducts/S1/2017/VHVV_stack_32630_subset.tif'
SAR.stack <- crop(SAR.stack, extent(shp_utm))
writeRaster(SAR.stack, filename=out.name, format='GTiff', Ebandorder='TIF', datatype='FLT4S', overwrite=TRUE, crs=in.VV.d@crs)

for (i in 1:length(SAR.stack@layers)) {
  SAR.stack[[i]] = 10*log10(raster(SAR.stack, layer=i))
}


out.name = 'L:/presales/ClaasRGF/04_DataProducts/S1/2017/VHVVdB_stack_32630_subset.tif'
writeRaster(SAR.stack, filename=out.name, format='GTiff', Ebandorder='TIF', datatype='FLT4S', overwrite=TRUE, crs=in.VV.d@crs)


# logNew = function(x) ifelse(x!=0, 10*log10(x), NA)

for (i in 1:in.VV.d@data@nlayers) {
  in.VV.d[[i]] = 10*log10(raster(in.VV.d, layer=i))
}

out.name = 'L:/presales/ClaasRGF/04_DataProducts/S1/2017/VVdB_stack_32630_subset.tif'
writeRaster(in.VV.d, filename=out.name, format='GTiff', Ebandorder='TIF', datatype='FLT4S', overwrite=TRUE, crs=in.VV.d@crs)


for (j in 1:in.VH.d@data@nlayers) {
  in.VH.d[[j]] = 10*log10(raster(in.VH.d, layer=j))
}
out.name = 'L:/presales/ClaasRGF/04_DataProducts/S1/2017/VHdB_stack_32630_subset.tif'
writeRaster(in.VH.d, filename=out.name, format='GTiff', Ebandorder='TIF', datatype='FLT4S', overwrite=TRUE, crs=in.VV.d@crs)




t2=Sys.time()



print('SCRIPT EXECUTED')
print(paste('Start time:',t1))
print(paste('End time:',t2))

