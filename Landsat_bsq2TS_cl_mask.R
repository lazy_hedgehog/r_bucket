
#+
# :Description:
#      code to create a time series multi-layer file from single files. Landsat edition. 
#      with cloud masking
#       
# :AUTHOR: Katarzyna Ewa Lewinska
# :DATE: 25 April 2017
# :VERSION: 1.10
#-


########CHANGE######
#
veg.idx = 'NDVI'
#
####################


t1=Sys.time()

library(raster)
library(rgdal)


setwd('Z:/WestAustralia/landsat/wetseason')
(wd <- getwd())

in.list <- list.files(path=wd, pattern=paste0('*',veg.idx,'.tif$'), full.names=TRUE, recursive = TRUE)

# in.image = raster(in.list[1]) # open dummy file for shp
# # im.crs = in.image@crs


### PROJECTION MASTER: HERE HANDLING OF S HEMISPHERE IS CORRECT AND CONSISTENT WITH SHP 
master=raster('Z:/WestAustralia/landsat/wetseason/ENVI_projection_master.bsq')

in.shp = 'Z:/WestAustralia/Client_Data/MidwestRezatec_ID.shp'
for.pol = readOGR(dsn=in.shp, p4s=master@crs@projargs) # shp object

out.ts.file = paste0(wd,'/',veg.idx,'_mask_layerstack.bsq')


### Crop based on a shapefile
for (i in 1:length(in.list)){
    print(i)
    in.image = raster(in.list[i])
    in.image <- projectRaster(in.image, master)
    crop.im = crop(in.image,for.pol)
        
    ### look for a mask and if present, mask an image with it
    in.mask = list.files(dirname(in.list[i]), pattern=paste0('*','_mask.bsq$'), full.names=TRUE)
    if (length(in.mask) > 0){
        in.mask.im = raster(in.mask)
        crop.mask = crop(in.mask.im, for.pol)
        crop.im = (crop.im)*(crop.mask)
    } 
     
    
    out.name = paste0(substr(in.list[i],start=1,stop=(nchar(in.list[i])-4)),'_mask_crop.tif')
    
    writeRaster(crop.im, out.name, overwrite=TRUE, csr=crop.im@crs)
}

### Layerstack
in.crop.list <- list.files(path=wd, pattern=paste0('*',veg.idx,'_mask_crop.tif$'), full.names=TRUE, recursive = TRUE)

x.min=c()
x.max=c()
y.min=c()
y.max=c()

# feach extends of all input images
for (i in 1:length(in.crop.list)){
    e = extent(raster(in.crop.list[i]))
    
    x.min=append(x.min,e@xmin)
    x.max=append(x.max,e@xmax)
    y.min=append(y.min,e@ymin)
    y.max=append(y.max,e@ymax)
}

x.min=max(x.min)
x.max=min(x.max)
y.min=max(y.min)
y.max=min(y.max)

# define common area
crop.common = as(extent(x.min,x.max,y.max,y.min), 'SpatialPolygons')

# go back to images and crop once more to the sam,e extend
for (i in 1:length(in.crop.list)){
    
    in.image = raster(in.crop.list[i])
    
    crop.im = crop(in.image,crop.common)
    
    out.name = paste0(substr(in.list[i],start=1,stop=(nchar(in.list[i])-4)),'_mask_crop2.tif')
    
    dataType(crop.im) = 'FLT4S'
    
    writeRaster(crop.im, out.name, overwrite=TRUE, csr=crop.im@crs, datatype='FLT4S')
    
}

### sort layers 
b.names = substr(basename(in.list),start=18,25)
sort.idx = sort(b.names, index.return=TRUE)$ix
b.names=b.names[sort.idx]


in.crop.list <- list.files(path=wd, pattern=paste0('*',veg.idx,'_mask_crop2.tif$'), full.names=TRUE, recursive = TRUE)

order.list = in.crop.list[sort.idx]

list.stack = stack(in.crop.list[sort.idx])

# dataType(list.stack) = 'FLT4S'



writeRaster(list.stack, filename=out.ts.file, format = 'ENVI', Ebandorder='BSQ', datatype='FLT4S', overwrite=TRUE, crs=list.stack@crs)


### EDIT HEADER INFORMATION ADDING BAND NAMES
### AT THIS POINT DONE MANUALLY
stack.hdr <- readLines(paste0(wd,'/',veg.idx,'_mask_layerstack.hdr'))
stack.hdr <- gsub("Band 1,", b.names[1], stack.hdr) 
stack.hdr <- gsub("Band 2,", b.names[2], stack.hdr)
stack.hdr <- gsub("Band 3,", b.names[3], stack.hdr) 
stack.hdr <- gsub("Band 4,", b.names[4], stack.hdr)  
stack.hdr <- gsub("Band 5,", b.names[5], stack.hdr)  
stack.hdr <- gsub("Band 6,", b.names[6], stack.hdr)  
stack.hdr <- gsub("Band 7,", b.names[7], stack.hdr)  
stack.hdr <- gsub("Band 8,", b.names[8], stack.hdr)  
stack.hdr <- gsub("Band 9,", b.names[9], stack.hdr)  
stack.hdr <- gsub("Band 10,", b.names[10], stack.hdr)  
stack.hdr <- gsub("Band 11,", b.names[11], stack.hdr)  
stack.hdr <- gsub("Band 12,", b.names[12], stack.hdr)  
stack.hdr <- gsub("Band 13,", b.names[13], stack.hdr)  
stack.hdr <- gsub("Band 14,", b.names[14], stack.hdr)  
stack.hdr <- gsub("Band 15,", b.names[15], stack.hdr)  
stack.hdr <- gsub("Band 16,", b.names[16], stack.hdr)  
stack.hdr <- gsub("Band 17,", b.names[17], stack.hdr)  
stack.hdr <- gsub("Band 18,", b.names[18], stack.hdr)  
stack.hdr <- gsub("Band 19,", b.names[19], stack.hdr)  
stack.hdr <- gsub("Band 20,", b.names[20], stack.hdr)  
stack.hdr <- gsub("Band 21", b.names[21], stack.hdr) 
cat(stack.hdr, file=paste0(wd,'/',veg.idx,'_mask_layerstack.hdr'), sep="\n") #overwrites the old ENVI header


t2=Sys.time()

print('SCRIPT EXECUTED')
print(paste('Start time:',t1))
print(paste('End time:',t2))

