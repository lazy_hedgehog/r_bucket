
#+
# :Description:
#
# :AUTHOR: Katarzyna Ewa Lewinska
# :DATE: 18 June 2018
# :VERSION: 1.0
#-


t1=Sys.time()


library(rgdal)
library(sp)
library(raster)
library(ggplot2)
library(stringr)


######

nu.f <- function(x){
  as.numeric(x)
}

sum.na <- function(x){
  y=sum(x, na.rm = TRUE)
}

######



TILE =  c('30VWJ', '30UVD', '30UXC', '30UWB', '30VUJ', '30VVJ')

in.shp = 'L:/research/uk_forestry/01_SourceData/20180619_Forest_DB_location_assessment.shp'
shp.f = readOGR(dsn=in.shp) 


in.dir = 'K:/global_data/uk/s2/s2data'
out.dir = 'L:/research/uk_forestry/S2_timeseries4fieldwork'


out.csv <- paste0(out.dir, '/', 'Alltiles_Forest_BD_loc_as_time_series.csv')
out.csv2 <- paste0(out.dir, '/', 'Alltiles_Forest_BD_loc_as_time_series_BINARY.csv')
out.csv3 <- paste0(out.dir, '/', 'Alltiles_Forest_BD_loc_as_time_series_species_summary.csv')
plot_name = paste0(out.dir, '/', 'Alltiles_Forest_BD_loc_as_time_series_species_summary.pdf')



tab <- as.data.frame(matrix(c("AH", "CP", "DF", "EL", "HL", "JL", "LP", "NS", "SP", "SS")))
colnames(tab) <- 'SP'

for (i in 1:length(TILE)){
  
  in.dir.t = paste0(in.dir,'/',TILE[i])
  r.list <- list.files(path=in.dir.t, pattern="*_B04.tif$", full.names = TRUE, include.dirs = TRUE, recursive = TRUE)
  
  s <- raster::stack(r.list)
  
  shp.f <- spTransform(shp.f, crs(raster(r.list[1])))
  shp.t <- crop(shp.f, extent(raster(r.list[1])))

  shp.t <- shp.f[shp.f$LOC_AS=='Y',]  

  ts <- extract(s, shp.t, df=TRUE)
  
  ts.b <- ts[2:ncol(ts)]
  ts.b = apply(ts.b, c(1,2), nu.f)
  ts.b[!is.na(ts.b)] = 1
  
  n <- substring(r.list, 41,48)
  

  df.b <- cbind.data.frame(shp.t@data$INIQ_ID, shp.t@data$Species1, ts.b)
  names(df.b) <-c('INIQ_ID', 'Species', n)
    
  
  sum.df = c()
  for(i in 1:length(levels(df.b$Species))) {
    ssum = apply(df.b[df.b$Species==levels(df.b$Species)[i],][,3:ncol(df.b)], 2, FUN=sum.na)
    
    sum.df <- rbind(sum.df, ssum)
  }
  rownames(sum.df) <- levels(df.b$Species)
  
  sum.df <- as.data.frame(sum.df[,1:ncol(sum.df)])
  
  sum.df[sum.df==0] <- NA
  
  sum.df$SP = rownames(sum.df)
  
  tab <- merge(tab,sum.df, all = T, by = 'SP')
    
} 

# dates.S2 <- as.Date(substring(colnames(tab[2:ncol(tab)]),1,8), format="%Y %m %d")
# uni.dates.S2 <- unique(dates.S2)

dates.S2 <- substring(colnames(tab[2:ncol(tab)]),1,8)
uni.dates.S2 <- unique(dates.S2)


sum.df = as.data.frame(tab$SP)
names(sum.df) <- 'SP'

for (j in 1:length(uni.dates.S2)){
  temp.col <- tab[str_detect(names(tab), uni.dates.S2[j])]
  temp.col <- apply(temp.col, 1, FUN=sum.na)
  
  sum.df <- cbind(sum.df, temp.col)
}

names(sum.df) <- c('SP', uni.dates.S2)
sum.df[sum.df==0] <- NA
ud <- as.Date(uni.dates.S2, format="%Y %m %d")

### PLOTS



pdf(plot_name)

# plot(ud, sum.df[1,2:ncol(sum.df)], type = 'p', col = "cyan", pch = 15, ylim=c(0,max(sum.df[,2:ncol(sum.df)], na.rm = TRUE)+10), cex=1,
#      xlab = '2017', ylab='frequency',
#      main ='S2 acquisitions depicting sampling plots')
# points(ud, sum.df[2,2:ncol(sum.df)], type = 'p', col = "red", pch = 15)
# points(ud, sum.df[3,2:ncol(sum.df)], type = 'p', col = "green", pch = 16)
# points(ud, sum.df[4,2:ncol(sum.df)], type = 'p', col = "blue", pch = 17)
# points(ud, sum.df[5,2:ncol(sum.df)], type = 'p', col = "gray", pch = 18)
# points(ud, sum.df[6,2:ncol(sum.df)], type = 'p', col = "darkorange", pch = 19, cex=.8)
# points(ud, sum.df[7,2:ncol(sum.df)], type = 'p', col = "black", pch = 15, cex=.8)
# points(ud, sum.df[8,2:ncol(sum.df)], type = 'p', col = "deepskyblue1", pch = 16, cex=.8)
# points(ud, sum.df[9,2:ncol(sum.df)], type = 'p', col = "darkmagenta", pch = 17, cex=.8)
# points(ud, sum.df[10,2:ncol(sum.df)], type = 'p', col = "brown", pch = 18, cex=.8)
# 
# 
# legend("topleft", legend=sum.df$SP, inset=c(0,0), title="Species", horiz=FALSE, 
#        pch = c(15, 15, 16, 17, 18, 19, 15, 16, 17, 18), pt.lwd = c(.0,.0,.0,.0,.0,.0,.0,.0,.0),
#        col=c("cyan","red","green","blue","gray","darkorange","black","deepskyblue1","darkmagenta","brown"),
#        lty=1:2, cex=0.7, pt.cex=0.7, ncol=2)


plot(ud, sum.df[1,2:ncol(sum.df)], type = 'p', col = "cyan", pch = 15, ylim=c(0,max(sum.df[,2:ncol(sum.df)], na.rm = TRUE)+10), cex=1,
     xlab = '2017', ylab='frequency',
     main ='S2 acquisitions depicting sampling plots')
points(ud, sum.df[2,2:ncol(sum.df)], type = 'p', col = "red", pch = 15)
points(ud, sum.df[3,2:ncol(sum.df)], type = 'p', col = "green", pch = 16)
points(ud, sum.df[4,2:ncol(sum.df)], type = 'p', col = "blue", pch = 17)
points(ud, sum.df[5,2:ncol(sum.df)]+sum.df[6,2:ncol(sum.df)], type = 'p', col = "darkorange", pch = 19)
# points(ud, sum.df[6,2:ncol(sum.df)], type = 'p', col = "darkorange", pch = 19, cex=.8)
points(ud, sum.df[7,2:ncol(sum.df)], type = 'p', col = "black", pch = 15, cex=.8)
points(ud, sum.df[8,2:ncol(sum.df)], type = 'p', col = "deepskyblue1", pch = 16, cex=.8)
points(ud, sum.df[9,2:ncol(sum.df)], type = 'p', col = "darkmagenta", pch = 17, cex=.8)
points(ud, sum.df[10,2:ncol(sum.df)], type = 'p', col = "brown", pch = 18, cex=.8)


legend("topleft", legend=c('AH','CP', 'DF', 'EL','HL+JL', 'LP', 'NS', 'SP', 'SS'), inset=c(0,0), title="Species", horiz=FALSE, 
       pch = c(15, 15, 16, 17, 19, 15, 16, 17, 18), pt.lwd = c(.0,.0,.0,.0,.0,.0,.0,.0),
       col=c("cyan","red","green","blue","darkorange","black","deepskyblue1","darkmagenta","brown"),
       lty=1:2, cex=0.7, pt.cex=0.7, ncol=2)

dev.off() 

t2=Sys.time()



print('SCRIPT EXECUTED')
print(paste('Start time:',t1))
print(paste('End time:',t2))
