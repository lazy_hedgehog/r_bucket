
#+
# :Description:to extract pixel valued for plugons. WAPRES project
#              Select only polygons that have the same age and simillar stocking as the target
#
# :AUTHOR: Katarzyna Ewa Lewinska
# :DATE: 10 October 2017
# :VERSION: 1.0
#-


t1=Sys.time()

library(raster)
library(rgdal)
library(sp)
# library(rgeos)


setwd('Y:/000045_WAPRES/04_DataProducts/L0/S2/T50HMH')
(wd <- getwd())

### REFERENCE

### get image
in.im = 'Y:/000045_WAPRES/04_DataProducts/L0/S2/T50HMH/S170226_layerstack.bsq'
# in.im = 'Y:/000045_WAPRES/S2A_MSIL2A_20170226T021911_N0204_R060_T50HMH_20170226T022243.SAFE/L2A_T50HMH_20170226T021911_layerstack.bsq'
in.data = brick(in.im)


in.ref.shp = 'X:/projects/000045_WAPRES/04_DataProducts/L1/Stress/12066_IN08_testside_corepixels.shp'
shp.ref = readOGR(dsn=in.ref.shp)
### get shp database information and change projection
shp.ref.utm = spTransform(shp.ref, in.data@crs)


### TARGET
in.shp = 'X:/projects/000045_WAPRES/02_ClientData/Stress/Target_KARAFILIS_plantation/KARAFILIS_Resource.shp'
shp.dat = readOGR(dsn=in.shp)
### get shp database information and change projection
shp.utm = spTransform(shp.dat, in.data@crs)


### CONTROL 
out = extract(in.data, shp.ref.utm, method='simple', buffer=NULL, small=FALSE, cellnumbers=TRUE, df=TRUE)

pol.ref = c(2,6,8,9,10,12,17)

out.df=data.frame(0,0,0,0,0,0,0,0,0,0,0,0)
names(out.df) = c(unlist(names(out)))

for(i in pol.ref){
  
  out.pol = out[out$ID==i,]
  
  out.df = merge(out.df,out.pol, all=TRUE)
}

out.df=out.df[which(out.df$ID > 0),]



### add indices
out.df$NDVI=((out.df$B8-out.df$B4)/(out.df$B8 + out.df$B4))
out.df$NDII=((out.df$B8-out.df$B11)/(out.df$B8 + out.df$B11))
out.df$GRE = ((-0.2848*out.df$B2)-(0.2435*out.df$B3)-(0.5436*out.df$B4)+(0.7243*out.df$B8)+(0.0840*out.df$B11)-(0.1800*out.df$B12))


# jpeg(filename="X:/projects/000045_WAPRES/04_DataProducts/L1/Stress/Trial_plots_core_20170226_NDVI.jpg", width = 1000, height = 550, units = "px")
# boxplot(out$NDVI~out$ID)
# dev.off()
# 
# jpeg(filename="X:/projects/000045_WAPRES/04_DataProducts/L1/Stress/Trial_plots_core_20170226_NDII.jpg", width = 1000, height = 550, units = "px")
# boxplot(out$NDII~out$ID)
# dev.off()
# 
# jpeg(filename="X:/projects/000045_WAPRES/04_DataProducts/L1/Stress/Trial_plots_core_20170226_GRE.jpg", width = 1000, height = 550, units = "px")
# boxplot(out$GRE~out$ID)
# dev.off()




### out is a dataframe where all pixels within polygons are given. ID informs on a polygon while cell on indivual pixel. 
out.target = extract(in.data, shp.ref.utm, method='simple', buffer=NULL, small=FALSE, cellnumbers=TRUE, df=TRUE)


# uniq=as.numeric(unlist(unique(out.target[1])))
# 
# data.f.t=data.frame(0,0,0,0,0,0)
# names(data.f.t) <- c('polygon', 'mean', 'sd','min','max','median')
# 
# # for(i in uniq){
# #   
# #   out.pol.t = out[out.target$ID==i,]
# #   
# #   p.mean <- mean(as.numeric(unlist(out.pol.t[9])))
# #   p.sd <- sd(as.numeric(unlist(out.pol.t[9])))
# #   p.min <- min(as.numeric(unlist(out.pol.t[9])))
# #   p.max <- max(as.numeric(unlist(out.pol.t[9])))
# #   p.median <- median(as.numeric(unlist(out.pol.t[9])))
# #   
# #   data.f.t=rbind.data.frame(data.f.t, c(i,p.mean,p.sd,p.min,p.max,p.median))
# # }
# 
# 
# # filename = 'X:/projects/000045_WAPRES/04_DataProducts/L1/Stress/Target_pol_20170226_B8.csv'
# # write.table(data.f.t, filename, sep = ",", col.name = T)
# # 
# # # jpeg(filename="X:/projects/000045_WAPRES/04_DataProducts/L1/Stress/Baseline_polygons_20161208_B8.jpg", width = 900, height = 550, units = "px")
# # boxplot(out.target$B8~out.target$ID)
# # dev.off()
# 
# 
# out.target.2 = out.target
# out.target.2$ID = out.target.2$ID+100
# combined = rbind.data.frame(out.df[1:12], out.target.2[which(out.target.2$ID > 108),])
# 
# jpeg(filename="X:/projects/000045_WAPRES/04_DataProducts/L1/Stress/Combined_selested_target_20170226_B8.jpg", width = 1000, height = 550, units = "px")
# boxplot(combined$B8~combined$ID)
# dev.off()
# 
# jpeg(filename="X:/projects/000045_WAPRES/04_DataProducts/L1/Stress/Combined_selected_target_20170226_B4.jpg", width = 1000, height = 550, units = "px")
# boxplot(combined$B4~combined$ID)
# dev.off()
# 
# jpeg(filename="X:/projects/000045_WAPRES/04_DataProducts/L1/Stress/Combined_selected_target_20170226_B12.jpg", width = 1000, height = 550, units = "px")
# boxplot(combined$B12~combined$ID)
# dev.off()
# 
# 
# ### add indices
# combined$NDVI=((combined$B8-combined$B4)/(combined$B8 + combined$B4))
# combined$NDII=((combined$B8-combined$B11)/(combined$B8 + combined$B11))
# combined$GRE = ((-0.2848*combined$B2)-(0.2435*combined$B3)-(0.5436*combined$B4)+(0.7243*combined$B8)+(0.0840*combined$B11)-(0.1800*combined$B12))
# 
# combined=combined[which(combined$ID > 0),]
# 
# jpeg(filename="X:/projects/000045_WAPRES/04_DataProducts/L1/Stress/Combined_selected_target_20170226_NDVI.jpg", width = 1000, height = 550, units = "px")
# boxplot(combined$NDVI~combined$ID)
# dev.off()
# 
# jpeg(filename="X:/projects/000045_WAPRES/04_DataProducts/L1/Stress/Combined_selected_target_20170226_NDII.jpg", width = 1000, height = 550, units = "px")
# boxplot(combined$NDII~combined$ID)
# dev.off()
# 
# jpeg(filename="X:/projects/000045_WAPRES/04_DataProducts/L1/Stress/Combined_selected_target_20170226_GRE.jpg", width = 1000, height = 550, units = "px")
# boxplot(combined$GRE~combined$ID)
# dev.off()


t2=Sys.time()



print('SCRIPT EXECUTED')
print(paste('Start time:',t1))
print(paste('End time:',t2))

